---
layout: job_page
title: "Production Engineer"
---

A production engineer is a developer with deep knowledge of some parts of the stack, either it
is networking, or the linux kernel, or even an specific interesting in scaling and algorithms.

It could also be seen as any systems engineer who aims to code themselves out of a job by automating
all the things.

## Responsibilities

* As an Production Engineer you will be on a pager duty rotation to respond
to GitLab.com availability incidents and support for service engineers with
customer incidents.
* Improve GitLab.com availability and performance.
* Incident response for GitLab.com.
* Making GitLab easier to maintain for administrators all over the world.
* Manage our infrastructure with Chef (and a little Ansible)
* Build monitoring systems so alerts trigger on symptoms and not on outages.
* Improve the deployment process to make it as boring as possible.
* Scaling GitLab to support hundred of thousands of concurrent users.
* Work comfortably at any level of the stack.

## Workflow

* You work on issues in the [infrastructure repository](https://gitlab.com/gitlab-com/infrastructure/issues).
* The priority of the issues can be found in [the handbook under GitLab Workflow](https://about.gitlab.com/handbook/#prioritize).

## Requirements for Applicants

* Linux depth of knowledge (we use Ubuntu Server)
* Database scaling depht of knowledge (we use PostgreSQL and Redis)
* Chef experience (optional if you are awesome on something else)
* Ruby scripting experience (our preferred language for operations scripts)
* Programming expertise (Ruby and Ruby on Rails preferred; for GitLab debugging)
* Collaborative team spirit with great communication skills.
* Proactive, go for attitude. When you see something broken, you can't help fixing it.
* You share our [values](/handbook/#values), and work in accordance with those values.
