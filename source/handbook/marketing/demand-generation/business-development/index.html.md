---
layout: markdown_page
title: "Business Development"
---
BDR Handbook

[Up one level to the Demand Generation Handbook](/handbook/marketing/demand-generation/)    

## On this page
* [Customer FAQ's](#FAQ)  
* [Variable Compensation Guidelines](#compensation) 
* [Vacation Incentives](#vacation)

## Customer FAQ's<a name="FAQ"></a>  


BDRs are frequently asked the following questions in some form or another:  

* I didn’t get a confirmation email, what should I do?  
* I need to change my password, or my password change didn’t work, what should I do?  
* I want to use [username] but it says it’s already taken, even though that user has never actually done anything on his/her account. Can you remove that user so I can get the username? 
* I made a mistake when setting up my account and need to change it. What should I do?
* GitLab.com seems slow. Why is that? Is anything being done to address this?
* I'm migrating from GitHub, BitBucket, SVN, etc. Do you have any migration tools?
* I'm looking to integrate with a Kanban board or project management tool. What do you recommend? What integrates? Is GitLab's project management functionality any good? Do you have any resources that address project management within GitLab?
* I really need [specific feature, integration, etc.]. Is that planned?
* I don't think [Feature X] works like it should. I think it should work [this way]. Can you fix it?
* What tools do you integrate with? Do you integrate with [Tool X]?
* How do groups work within GitLab? How do permissions work within groups? How many users can be in a group?
* Do read-only users still count toward our total user count for EE? What about bots?
* What are the different levels of permission within a project? Can I limit access for certain projects to specific users?
* Can I allow specific people to access my private projects, while keeping the project private?
* Does GitLab have git LFS or git annex functionality?
* I’m working with PostgreSQL and/or PostGIS. Is this compatible with GitLab? How does GitLab work with these?
* How many repositories can I have on my GitLab account?
* Are you really free? Will you really be free forever?
* How much does it cost for XX enterprise licenses? Do you send an invoice or can I pay by credit card?
* Do you provide any volume discounts?
* Do you provide any reseller discounts?
* There are some EE features we would like to use, but not all of them - can we pay per feature and add them to CE?
* How is GitLab.com backed up and secured?

## Variable Compensation Guidelines<a name="compensation"></a>

Full-time BDRs have a significant portion of their pay based on performance and objective attainment. Performance based "bonuses" are based on results. Actions for obtaining results will be prescribed and measured, but are intentionally left out of bonus attainment calculations to encourage experimentation. BDRs will naturally be drawn to activities that have the highest yield, but freedom to choose their own daily activities will allow new, higher yield activities to be discovered.

Guidelines for bonuses:
* Team and individual objectives are based on GitLab's revenue targets and adjusted monthly. 
* Bonuses are paid quarterly.
* Bonuses may be based on various objectives
    * E.g. a new BDR's first month's bonus is typically based on completing <a href="https://about.gitlab.com/handbook/general-onboarding/" target="_blank">onboarding</a>
    * Bonuses may be tied to sales target attainment, <a href="https://about.gitlab.com/handbook/marketing/#okrs" target="_blank">OKRs</a>, or other objectives.

## BDR Vacation Incentives<a name="vacation"></a>

Note: this is a new, experimental policy started in June 2016: changes and adjustments are expected

At GitLab, we have an <a href="https://about.gitlab.com/handbook/#paid-time-off" target="_blank">unlimited time off policy</a>. The business development team has additional incentives for taking time off:
1. The first 5 business days taken off by a BDR in a calendar year prorates their target by 100%, rounded up (i.e. reduces the BDR's target by the period's target divided by the number of business days in the period, times the number of days taken off).
2. The second 5 business days are prorated by 50%, rounded up if applicable
3. The third 5 business days are prorated by 25%, rounded up if applicable

E.g. A BDR with a target of 200 opportunities in a January with 20 business days can reduce that month's target to 190 by taking 1 day off, 150 by taking 5 days off, 125 by taking 10 days off, or 113 by taking 15 days off.

Rules and qualifications for BDR Vacation Incentives:

* BDRs must give notice 30 days ahead of intended time off to by the Team Lead and Senior Demand Generation Manager (and ensure they acknowledge receipt within 48 hours)
* BDRs must add intended time off to the availability calendar
* Prorated days can be deferred (i.e. take PTO without prorating the target), but are lost at the end of the year. Incidentally, the math usually works out in favor of not deferring.
* BDRs can take additional vacation days (see <a href="https://about.gitlab.com/handbook/#paid-time-off" target="_blank">GitLab PTO policy</a>), but time off beyond 15 days will not adjust targets.

With <a href="https://about.gitlab.com/handbook/#paid-time-off" target="_blank">GitLab's PTO policy</a>), there's no need to give notice or ask permission to take time off. Unfortunately, a quota carrying BDR can expect to lose some earning potential for taking PTO, which often leads to BDRs not taking vacation. Another problem with unlimited PTO is that management loses forecasting power when BDRs take unexpected PTO, making it tempting for leadership to discourage vacation. We hope that this additional vacation incentive for the team acts as a balancing incentive to both parties by (a) providing a prorated quota for BDRs who plan ahead and (b) helping leadership plan and forecast better without discouraging vacation.