---
layout: markdown_page
title: "On-boarding process for Premium Support"
---

# On-boarding process for Premium Support
After purchase of a Premium Support subscription is completed, the following steps will take place:

- Sales verifies that the all the necessary contact information exists in the SalesForce record for the customer
   - Email address is mandatory
- Sales opens a confdential issue in the support project asking for an Service Engineer to be assigned
- Sales notifies the designated Support person who processes the issue.
- Sales sends the customer an email message with the [text contained here](premium_support_message.txt) so the customer knows what to expect.
- Support assigns an Service Engineer based on a number of factors
   - Location
   - Availability
   - SLA
- Assigned support engineer contacts customer
   - Provides information on contacting support
