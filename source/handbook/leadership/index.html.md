---
layout: markdown_page
title: Leadership
---

## Behavior

- As a leader team members will follow your behavior, always do the right thing.
- Behavior should be consistent inside and outside the company, don't fake it outside, just do the right thing inside the company as well.

## Articles

- [eShares Manager’s FAQ](https://readthink.com/a-managers-faq-35858a229f84)
- [How Facebook Tries to Prevent Office Politics](https://hbr.org/2016/06/how-facebook-tries-to-prevent-office-politics)
- [The Management Myth](http://www.theatlantic.com/magazine/archive/2006/06/the-management-myth/304883/)
- [Later Stage Advice for Startups](http://themacro.com/articles/2016/07/later-stage-advice-for-startups/)

## Books

- High output management - Andrew Grove
- The Hard thing about hard things - Ben Horowitz
- [The score takes care of itself - Bill Walsh](http://coachjacksonspages.com/The%20Score%20Takes%20Care.pdf)
